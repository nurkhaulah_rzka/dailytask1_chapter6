/**
 * @file contains entry point of controllers api v1 module
 * @author Nur Khaulah Arrizka
 */

const postController = require("./postController");
const userController = require("./userController");

// Exports module:
module.exports = {
  postController,
  userController,
};
