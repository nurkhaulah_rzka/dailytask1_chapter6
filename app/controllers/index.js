/**
 * @file contains entry point of controllers module
 * @author Nur Khaulah Arrizka
 */

const api = require("./api");

module.exports = {
  api,
};
