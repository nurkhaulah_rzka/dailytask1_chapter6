/**
 * @file Bootstrap express.js server
 * @author Nur Khaulah Arrizka
 */

const express = require("express");
const morgan = require("morgan");
const router = require("../config/routes");

const app = express();

/** Untuk install request logger */
app.use(morgan("dev"));

/** Untuk install JSON request parser */
app.use(express.json());

/** Untuk install Router */
app.use(router);

module.exports = app;
