const { User } = require("../models");
const bcrypt = require('bcrypt');

module.exports = {
  create(createArgs) {
    return User.create(createArgs);
  },

  update(id, updateArgs) {
    return User.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return User.destroy({
      where: {
        id
      }
    });
  },

  find(id) {
    return User.findByPk({
      attributes:  {exclude: ['password']},
      where: {
        id,
      }    
    });
  },

  findAll() {
    return User.findAll({attributes:  {exclude: ['password']},});
  },

  getTotalUser() {
    return User.count();
  },

  // to register a new user
  async registerNewUser(createArgs) {
    console.log('Ini adalah repository.')

    const user = await User.findOne({
      where: {
        email : createArgs.email
      }
    })

    // to check if the user doesn't exist
    if(user){
      if (user.email === createArgs.email) {
        throw new Error(`Sorry, user with email : ${user.email} has already been taken!`)
      }
    }
    return User.create(createArgs);

  },

  async login(userArgs) {
    console.log(userArgs)
    const user = await User.findOne({
      where: {
        email: userArgs.email
      }
    });

    if(user){
      const validPass = await bcrypt.compare(userArgs.password, user.password);
      if(validPass){
        console.log("Anda telah berhasil masuk.")
        return User.findOne({
          attributes: {exclude: ['password']},
          where: {
            email: userArgs.email,
          }    
        });
      }else{
          throw new Error("Maaf, password yang Anda masukkan salah!")
      }
    }else{
      throw new Error("Maaf, user tidak ditemukan!")
    }

  }
};

// ------tes---------
// module.exports = {
//   create
//   register
//   fafwfwfw
// }
